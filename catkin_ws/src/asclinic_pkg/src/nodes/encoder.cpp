// Copyright (C) 2021, The University of Melbourne, Department of Electrical and Electronic Engineering (EEE)
//
// This file is part of ASClinic-System.
//    
// See the root of the repository for license details.
//
// ----------------------------------------------------------------------------
//     _    ____   ____ _ _       _          ____            _                 
//    / \  / ___| / ___| (_)____ (_) ___    / ___| _   _ ___| |_ ___ ________  
//   / _ \ \___ \| |   | | |  _ \| |/ __|___\___ \| | | / __| __/ _ \  _   _ \ 
//  / ___ \ ___) | |___| | | | | | | (_|_____|__) | |_| \__ \ ||  __/ | | | | |
// /_/   \_\____/ \____|_|_|_| |_|_|\___|   |____/ \__, |___/\__\___|_| |_| |_|
//                                                 |___/                       
//
// DESCRIPTION:
// Template node for mointoring edge events on a GPIO pin
//
// ----------------------------------------------------------------------------

#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Int32.h"
#include "std_msgs/Float32.h"

#include <gpiod.h>

// Includes required for threading
// > Mutex for handling shared variable
#include <mutex>
// > Include only one of the following two options:
#include <thread>
//#include <boost/thread/thread.hpp>

// Include the asclinic message types
#include "asclinic_pkg/LeftRightInt32.h"
#include "asclinic_pkg/LeftRightFloat32.h"


// MEMBER VARIABLES FOR THIS NODE:
// > Publisher and timer for the current
//   encoder counts
ros::Publisher m_encoder_counts_publisher;
ros::Publisher encoder_left;
ros::Publisher encoder_right;
ros::Timer m_timer_for_publishing;

// > For sharing the encoder counts between nodes

// accumulative counts
int a_encoder_counts_for_motor_left_a  = 0;
int a_encoder_counts_for_motor_left_b  = 0;
int a_encoder_counts_for_motor_right_a = 0;
int a_encoder_counts_for_motor_right_b = 0;

// periodical counts
int p_encoder_counts_for_motor_left_a  = 0;
int p_encoder_counts_for_motor_left_b  = 0;
int p_encoder_counts_for_motor_right_a = 0;
int p_encoder_counts_for_motor_right_b = 0;

// > Mutex for preventing multiple-access of shared variables
std::mutex m_counting_mutex;

// > The line numbers to read
int m_line_number_for_motor_left_channel_a = 130;
int m_line_number_for_motor_left_channel_b = 84;
int m_line_number_for_motor_right_channel_a = 106;
int m_line_number_for_motor_right_channel_b = 105;

//
int direction_motor_left = 1;
int direction_motor_right = 1;

// // > Boolean flag for when to stop counting
bool encoder_thread_should_count = true;


// Define parameters for checking encoder counts
// how often the gpio pins are polled for count reading
float m_delta_t_for_publishing_counts = 0.02;



// Respond to timer callback
void timerCallbackForPublishing(const ros::TimerEvent&)
{
	// Get the current counts into a local variable
	// > And reset the shared counts variable to zero
	int counts_motor_left_a_local_copy;
	int counts_motor_left_b_local_copy;
	int counts_motor_right_a_local_copy;
	int counts_motor_right_b_local_copy;

	int p_counts_motor_left_a_local_copy;
	int p_counts_motor_left_b_local_copy;
	int p_counts_motor_right_a_local_copy;
	int p_counts_motor_right_b_local_copy;

	m_counting_mutex.lock();

	counts_motor_left_a_local_copy  = a_encoder_counts_for_motor_left_a;
	counts_motor_left_b_local_copy  = a_encoder_counts_for_motor_left_b;
	counts_motor_right_a_local_copy = a_encoder_counts_for_motor_right_a;
	counts_motor_right_b_local_copy = a_encoder_counts_for_motor_right_b;

	p_counts_motor_left_a_local_copy = p_encoder_counts_for_motor_left_a;
	p_counts_motor_left_b_local_copy = p_encoder_counts_for_motor_left_b;
	p_counts_motor_right_a_local_copy = p_encoder_counts_for_motor_right_a;
	p_counts_motor_right_b_local_copy = p_encoder_counts_for_motor_right_b;

	p_encoder_counts_for_motor_left_a  = 0;
	p_encoder_counts_for_motor_left_b  = 0;
	p_encoder_counts_for_motor_right_a = 0;
	p_encoder_counts_for_motor_right_b = 0;


	m_counting_mutex.unlock();

	// Publish a message
	asclinic_pkg::LeftRightInt32 msg;
	msg.left  = p_counts_motor_left_a_local_copy  + p_counts_motor_left_b_local_copy;
	msg.right = p_counts_motor_right_a_local_copy + p_counts_motor_right_b_local_copy;
	m_encoder_counts_publisher.publish(msg);

	std_msgs::Int32 msg1;
	msg1.data = counts_motor_left_a_local_copy  + counts_motor_left_b_local_copy;
	encoder_left.publish(msg1);

	std_msgs::Int32 msg2;
	msg2.data = counts_motor_right_a_local_copy  + counts_motor_right_b_local_copy;
	encoder_right.publish(msg1);

}




void encoderCountingThreadMain()
{
	// Specify the chip name of the GPIO interface
	// > Note: for the 40-pin header of the Jetson SBCs, this
	//   is "/dev/gpiochip0"
	const char * gpio_chip_name = "/dev/gpiochip1";

	// Make a local copy of the line number member variables
	int line_number_left_a  = m_line_number_for_motor_left_channel_a;
	int line_number_left_b  = m_line_number_for_motor_left_channel_b;
	int line_number_right_a = m_line_number_for_motor_right_channel_a;
	int line_number_right_b = m_line_number_for_motor_right_channel_b;

	// Initialise a GPIO chip, line, and event objects
	struct gpiod_chip *chip;
	struct gpiod_line *line_left_a;
	struct gpiod_line *line_left_b;
	struct gpiod_line *line_right_a;
	struct gpiod_line *line_right_b;
	struct gpiod_line_bulk line_bulk;
	struct gpiod_line_event event;
	struct gpiod_line_bulk event_bulk;

	// Specify the timeout specifications
	// > The first entry is seconds
	// > The second entry is nano-seconds
	struct timespec timeout_spec = { 0, 10000000 };
	
	// Intialise a variable for the flags returned
	// by GPIO calls
	int returned_wait_flag;
	int returned_read_flag;

	// Get and print the value of the GPIO line
	// > Note: the third argument to "gpiod_ctxless_get_value"
	//   is an "active_low" boolean input argument.
	//   If true, this indicate to the function that active state
	//   of this line is low.
	int value;
	// > For left motor channel A
	value = gpiod_ctxless_get_value(gpio_chip_name, line_number_left_a, false, "foobar");
	ROS_INFO_STREAM("[TEMPLATE ENCODER READ MULTI THREADED] On startup of node, chip " << gpio_chip_name << " line " << line_number_left_a << " returned value = " << value);
	// > For left motor channel B
	value = gpiod_ctxless_get_value(gpio_chip_name, line_number_left_b, false, "foobar");
	ROS_INFO_STREAM("[TEMPLATE ENCODER READ MULTI THREADED] On startup of node, chip " << gpio_chip_name << " line " << line_number_left_b << " returned value = " << value);
	// > For right motor channel A
	value = gpiod_ctxless_get_value(gpio_chip_name, line_number_right_a, false, "foobar");
	ROS_INFO_STREAM("[TEMPLATE ENCODER READ MULTI THREADED] On startup of node, chip " << gpio_chip_name << " line " << line_number_right_a << " returned value = " << value);
	// > For right motor channel B
	value = gpiod_ctxless_get_value(gpio_chip_name, line_number_right_b, false, "foobar");
	ROS_INFO_STREAM("[TEMPLATE ENCODER READ MULTI THREADED] On startup of node, chip " << gpio_chip_name << " line " << line_number_right_b << " returned value = " << value);

	// Open the GPIO chip
	chip = gpiod_chip_open(gpio_chip_name);
	// Retrieve the GPIO lines
	line_left_a  = gpiod_chip_get_line(chip,line_number_left_a);
	line_left_b  = gpiod_chip_get_line(chip,line_number_left_b);
	line_right_a = gpiod_chip_get_line(chip,line_number_right_a);
	line_right_b = gpiod_chip_get_line(chip,line_number_right_b);
	// Initialise the line bulk
	gpiod_line_bulk_init(&line_bulk);
	// Add the lines to the line bulk
	gpiod_line_bulk_add(&line_bulk, line_left_a);
	gpiod_line_bulk_add(&line_bulk, line_left_b);
	gpiod_line_bulk_add(&line_bulk, line_right_a);
	gpiod_line_bulk_add(&line_bulk, line_right_b);

	// Display the status
	ROS_INFO_STREAM("[TEMPLATE ENCODER READ MULTI THREADED] Chip " << gpio_chip_name << " opened and lines " << line_number_left_a << ", " << line_number_left_b << ", " << line_number_right_a << " and " << line_number_right_b << " retrieved");

	// Request the line events to be mointored
	// > Note: only one of these should be uncommented
	gpiod_line_request_bulk_falling_edge_events(&line_bulk, "foobar");
	//gpiod_line_request_bulk_both_edges_events(&line_bulk, "foobar");

	// Enter a loop that endlessly monitors the encoders
	while (encoder_thread_should_count)
	{

		// Monitor for the requested events on the GPIO line bulk
		// > Note: the function "gpiod_line_event_wait" returns:
		//    0  if wait timed out
		//   -1  if an error occurred
		//    1  if an event occurred.
		returned_wait_flag = gpiod_line_event_wait_bulk(&line_bulk, &timeout_spec, &event_bulk);

		// Respond based on the the return flag
		if (returned_wait_flag == 1)
		{
			// Get the number of events that occurred
			int num_events_during_wait = gpiod_line_bulk_num_lines(&event_bulk);

			// Lock the mutex while before counting the events
			m_counting_mutex.lock();

			// Iterate over the event
			for (int i_event = 0; i_event < num_events_during_wait; i_event++)
			{
				// Get the line handle for this event
				struct gpiod_line *line_handle = gpiod_line_bulk_get_line(&event_bulk, i_event);

				// Get the number of this line
				unsigned int this_line_number = gpiod_line_offset(line_handle);

				// Read the event on the GPIO line
				// > Note: the function "gpiod_line_event_read" returns:
				//    0  if the event was read correctly
				//   -1  if an error occurred
				returned_read_flag = gpiod_line_event_read(line_handle,&event);

				// Respond based on the the return flag
				if (returned_read_flag == 0)
				{
					// Increment the respective count
					if (this_line_number == line_number_left_a){
						a_encoder_counts_for_motor_left_a = a_encoder_counts_for_motor_left_a + direction_motor_left;
						p_encoder_counts_for_motor_left_a = p_encoder_counts_for_motor_left_a + direction_motor_left;
					}
					else if (this_line_number == line_number_left_b){
						a_encoder_counts_for_motor_left_b = a_encoder_counts_for_motor_left_b + direction_motor_left;
						p_encoder_counts_for_motor_left_b = p_encoder_counts_for_motor_left_b + direction_motor_left;
					}
					else if (this_line_number == line_number_right_a){
						a_encoder_counts_for_motor_right_a = a_encoder_counts_for_motor_right_a + direction_motor_right;
						p_encoder_counts_for_motor_right_a = p_encoder_counts_for_motor_right_a + direction_motor_right;
					}
					else if (this_line_number == line_number_right_b){
						a_encoder_counts_for_motor_right_b = a_encoder_counts_for_motor_right_b + direction_motor_right;
						p_encoder_counts_for_motor_right_b = p_encoder_counts_for_motor_right_b + direction_motor_right;
					}
				} // END OF: "if (returned_read_flag == 0)"

			} // END OF: "for (int i_event = 0; i_event < num_events_during_wait; i_event++)"

			// Unlock the mutex
			m_counting_mutex.unlock();

		} // END OF: "if (returned_wait_flag == 1)"
	} // END OF: "while (true)"

	// Release the lines
	gpiod_line_release_bulk(&line_bulk);
	// Close the GPIO chip
	gpiod_chip_close(chip);
	// Inform the user
	ROS_INFO("[TEMPLATE ENCODER READ MULTI THREADED] Lines released and GPIO chip closed");
}

void MotorDirectionCallback(const asclinic_pkg::LeftRightFloat32& msg){
	if(msg.left >= 0 && msg.right >= 0) {
		direction_motor_left = 1;
		direction_motor_right = 1;
	}
	else if(msg.left < 0 && msg.right >= 0){
		direction_motor_left = -1;
		direction_motor_right = 1;
	}else if(msg.left >= 0 && msg.right < 0){
		direction_motor_left = 1;
		direction_motor_right = -1;
	}else{
		direction_motor_left = -1;
		direction_motor_right = -1;
	}

}


int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "Encoder");
	ros::NodeHandle nodeHandle("~");


	// Initialise a node handle and publish encoder counts
	std::string ns_for_group = ros::this_node::getNamespace();
	ros::NodeHandle nh_for_group(ns_for_group);
	ros::Subscriber motor_subscriber = nh_for_group.subscribe("set_motor_duty_cycle", 1, MotorDirectionCallback);
	m_encoder_counts_publisher = nh_for_group.advertise<asclinic_pkg::LeftRightInt32>("encoder_counts", 10, false);
	encoder_left = nh_for_group.advertise<std_msgs::Int32>("lwheel", 10, false);
	encoder_right = nh_for_group.advertise<std_msgs::Int32>("rwheel", 10, false);
	// Initialise a timer
	m_timer_for_publishing = nodeHandle.createTimer(ros::Duration(m_delta_t_for_publishing_counts), timerCallbackForPublishing, false);

	// Create thread for counting the encoder events
	std::thread encoder_counting_thread (encoderCountingThreadMain);

	// Spin the node
	ros::spin();

	// Join back the encoder counting thread
	encoder_counting_thread.join();

	return 0;
}

