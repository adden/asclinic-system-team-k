#!/usr/bin/env python
# -*- coding: utf-8 -*-
############################

import math
import rospy
import numpy as np
import scipy.interpolate as sp
from asclinic_pkg.msg import path
from std_msgs.msg import String

# Functions
def trajGenCB(data):
    spl = sp.UnivariateSpline(
        x = data.x,
        y = data.y,
        s = data.num)
    
    traj_x = np.linspace(data.x[0],data.x[-1],(data.num*4)).tolist()
    traj_y = spl(traj_x)
    traj_num = data.num*4

    traj = path()
    traj.x = traj_x
    traj.y = traj_x
    traj.num = traj_num

    trajPub.publish(traj)
    pass

# Main
if __name__ == '__main__':
    node_name = "PathPlanner"
    rospy.init_node(node_name)
    node_name_space = rospy.get_namespace()
    path_topic = node_name_space + "path"
    traj_topic = node_name_space + "trajectory"
    pathSub = rospy.Subscriber(path_topic, path, trajGenCB)

    trajPub = rospy.Publisher(traj_topic, path, queue_size=2)
    
    