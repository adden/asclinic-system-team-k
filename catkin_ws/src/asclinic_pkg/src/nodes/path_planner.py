#!/usr/bin/env python
# -*- coding: utf-8 -*-
############################

import rospy
import random
import sys
import math
import heapq
import csv
import numpy as np
import scipy.interpolate as sp
from asclinic_pkg.msg import cmdGoal
from asclinic_pkg.msg import path
from geometry_msgs.msg import Pose2D
from std_msgs.msg import String

#### Constants

file_path = '/home/asc03/asclinic-system/catkin_ws/src/asclinic_pkg/src/nodes/points.csv'


#### Classes

class Env:
    def __init__(self):
        self.file = open(file_path, 'r', newline='')
        self.parseFile()
        self.adjGen()

    def parseFile(self):
        self.coordList = []
        reader =  csv.reader(self.file)
        for row in reader:
            x = float(row[0])
            y = float(row[1])
            self.coordList.append((x, y))
    
    def adjGen(self):
        self.adjMatrix = []
        for node_a in self.coordList:
            row = []
            for node_b in self.coordList:
                dist = math.dist(node_a,node_b)
                if dist < math.sqrt(2): 
                    if ((node_a[0] == 9.5) & (node_b[0] == 10.5)):
                        row.append(0)
                    else: row.append(dist)
                else: row.append(0)
            self.adjMatrix.append(row)
   

class AStar:
    # initialise: Env, Start coord, goal coord, heap
    def __init__(self, start, goal):
        self.env = Env()
        self.start = self.approx_start(start)
        self.goal = self.approx_start(goal)

        self.pq = []
        self.visited = []
        self.parent = dict()
        self.g = dict()
        pass

    # searching
    def searching(self):
        """
        A_star Searching.
        :return: path, visited order
        """

        self.parent[self.start] = self.start
        self.g[self.start] = 0
        self.g[self.goal] = math.inf
        heapq.heappush(self.pq,
                       (self.f_value(self.start), self.start))

        while self.pq:
            _, s = heapq.heappop(self.pq)
            self.visited.append(s)

            if s == self.goal:  # stop condition
                break

            for s_n in self.get_neighbours(s):
                new_cost = self.g[s] + self.cost(s, s_n)

                if s_n not in self.g:
                    self.g[s_n] = math.inf

                if new_cost < self.g[s_n]:  # conditions for updating Cost
                    self.g[s_n] = new_cost
                    self.parent[s_n] = s
                    heapq.heappush(self.pq, (self.f_value(s_n), s_n))

        return self.extract_path(self.parent), self.visited

    def searching_repeated_astar(self, e):
        """
        repeated A*.
        :param e: weight of A*
        :return: path and visited order
        """

        path, visited = [], []

        while e >= 1:
            p_k, v_k = self.repeated_searching(self.start, self.goal, e)
            path.append(p_k)
            visited.append(v_k)
            e -= 0.5

        return path, visited

    def repeated_searching(self, s_start, s_goal, e):
        """
        run A* with weight e.
        :param s_start: starting state
        :param s_goal: goal state
        :param e: weight of a*
        :return: path and visited order.
        """

        g = {s_start: 0, s_goal: float("inf")}
        PARENT = {s_start: s_start}
        OPEN = []
        CLOSED = []
        heapq.heappush(OPEN,
                       (g[s_start] + e * self.heuristic(s_start), s_start))

        while OPEN:
            _, s = heapq.heappop(OPEN)
            CLOSED.append(s)

            if s == s_goal:
                break

            for s_n in self.get_neighbours(s):
                new_cost = g[s] + self.cost(s, s_n)

                if s_n not in g:
                    g[s_n] = math.inf

                if new_cost < g[s_n]:  # conditions for updating Cost
                    g[s_n] = new_cost
                    PARENT[s_n] = s
                    heapq.heappush(OPEN, (g[s_n] + e * self.heuristic(s_n), s_n))

        return self.extract_path(PARENT), CLOSED

    # get path
    def extract_path(self, PARENT):
        path = [self.goal]
        s = self.goal

        while True:
            s = PARENT[s]
            path.append(s)

            if s == self.start:
                break

        return list(path)

    # get neighbours
    def get_neighbours(self, node):
        nodeIndex = self.env.coordList.index(node)
        neighboursIndex = [i for i in range(len(self.env.adjMatrix[nodeIndex])) if self.env.adjMatrix[nodeIndex][i] > 0]
        return [self.env.coordList[j] for j in neighboursIndex]
    
    # find cost
    # cost between two adj points should be adjMat entry
    def cost(self, start, goal):
        startIndex = self.env.coordList.index(start)
        goalIndex = self.env.coordList.index(goal)
        return self.env.adjMatrix[startIndex][goalIndex]

    # sum cost of cost and heuristic
    def f_value(self, s):
        return self.g[s] + self.heuristic(s)
    
    # heuristic
    def heuristic(self, s):
        goal = self.goal  # goal node
        return math.hypot(goal[0] - s[0], goal[1] - s[1])

    # approximate start point to nearest node
    def approx_start(self, start):
        min_dist = math.inf
        index = 0
        for i in self.env.coordList:
            current = math.dist(i,start)
            if  current < min_dist:
                min_dist = current
                index = self.env.coordList.index(i)
        return self.env.coordList[index]
    
# Node class for path planner management
class ppNode:
    def __init__(self, start, goal):
    # Initialise a publisher
        topic_name = node_name_space + "path"
        self.pp_publisher = rospy.Publisher(topic_name, path, queue_size=1)
    # Initialise path variable
        self.path_var = path()
    # Initialise points
        self.start = start
        self.goal = goal

    def runAstar(self):
        # run Astar algorithm
        self.astar = AStar(self.start, self.goal)
        self.path_a, self.visited = self.astar.searching()
        # Assign values to ros format
        self.length = len(self.path_a)
        self.path_var.num = self.length
        self.path_var.X = [None] * self.length
        self.path_var.Y = [None] * self.length
        for i in range(self.length):
            self.path_var.X[i] = self.path_a[self.length-i-1][0]
            self.path_var.Y[i] = self.path_a[self.length-i-1][1] 

    # def gen_trajectory(self, data):
    #     spl = sp.UnivariateSpline(
    #         x = data.X,
    #         y = data.Y
    #         )
    
    #     traj_x = np.linspace(data.x[0],data.x[-1],(data.num*4)).tolist()
    #     traj_y = spl(traj_x)
    #     traj_num = data.num*4

        # traj = path()
        # traj.x = traj_x
        # traj.y = traj_y
        # traj.num = traj_num
        # return

    # run Astar and publish
    def Publish(self):
        self.runAstar()
        # traj_var = self.gen_trajectory(self.path_var)
        self.pp_publisher.publish(self.path_var)

### Functions

def SubCallback(input_Goal):
    global s_goal
    global s_start
    global node_name_space
     
    s_goal = (input_Goal.x, input_Goal.y)
    print(s_goal)
    pp_Node = ppNode(s_start,s_goal)
    pp_Node.Publish()

def odomCallback(msg):
    global s_start
    x = msg.x
    y = msg.y
    s_start = (x,y)

### Main

if __name__ == '__main__':
    global s_start
    global s_goal

    # Initialise the node
    node_name = "PathPlanner"
    rospy.init_node(node_name)
    node_name_space = rospy.get_namespace()
    # wait for start coords from localisation 

    # s_start = (1,1)
    # subscriber
    topic_name = "path"
    topic_inputgoal_name = "input_goal"
    rospy.Publisher(topic_name, path, queue_size=1)
    goalSub = rospy.Subscriber(topic_inputgoal_name, cmdGoal, SubCallback)

    odomSub = rospy.Subscriber('odometry_pose', Pose2D, odomCallback)

    # Start the path planner class

    rospy.spin()