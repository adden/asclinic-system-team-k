#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import rospy
import cv2
import numpy as np
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import time
from std_msgs.msg import UInt32


class CameraObjectDetector():


    def __init__(self):

        # Subscriber to the image frame from camera_capture.py
        self.sub = rospy.Subscriber("/asc/camera_image", Image, self.load_image_callback)

        self.net = cv2.dnn.readNetFromDarknet('/home/asc03/asclinic-system/catkin_ws/src/asclinic_pkg/src/nodes/yolo/v3tiny/yolov3-tiny.cfg', '/home/asc03/asclinic-system/catkin_ws/src/asclinic_pkg/src/nodes/yolo/v3tiny/yolov3-tiny.weights')
        self.net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)

        self.ln = self.net.getLayerNames()
        self.ln = [self.ln[i - 1] for i in self.net.getUnconnectedOutLayers()]

        self.t_pic = 0

        self.classes = open('/home/asc03/asclinic-system/catkin_ws/src/asclinic_pkg/src/nodes/yolo/coco.names').read().strip().split('\n') # Names of the classes
        np.random.seed(42)
        self.colors = np.random.randint(0, 255, size=(len(self.classes), 3), dtype='uint8') # Colours to use for the bounding boxes
        
        self.plant_pos_pub_x = rospy.Publisher('/plant_pos_x', UInt32, queue_size=5) # Return the x position of the

        self.yolo_active_sub = rospy.Subscriber("yolo_active", UInt32, self.activate_yolo_node)
        self.yolo_active = False # Whether this node is actively taking/processing images


        self.count = 1 # count for the images taken

    def activate_yolo_node(self, msg: UInt32):
        # Sets the yolo_active flag
        if msg.data == 1:
            self.yolo_active = True
        elif msg.data == 0:
            self.yolo_active = False



    # Callback when subscriber recieves a frame
    def load_image_callback(self, msg: Image):

        # if not self.yolo_active: # Dont bother with the image if yolo is not active
        #     return

        # Convert it into into a numpy array to process the image:
        img = CvBridge().imgmsg_to_cv2(msg) # 1920x1080x3

        blob = cv2.dnn.blobFromImage(img, 1/255.0, (416, 416), swapRB=True, crop=False)
        self.net.setInput(blob)

        outputs = self.net.forward(self.ln)        

        outputs = np.vstack(outputs)

        self.post_process(img, outputs, conf=0.25) # Set the yolo confidence threshold

        # cv2.imwrite('/home/asc03/asclinic-system/catkin_ws/src/asclinic_pkg/src/nodes/yolo/output.jpg', img) # Save the output image
        # print(f"Image processed in {t}") # Log how long it takes to process the image for debugging




    def post_process(self, img, outputs, conf):
        H, W = img.shape[:2]

        boxes = []
        confidences = []
        classIDs = []

        for output in outputs:
            scores = output[5:]
            classID = np.argmax(scores)
            confidence = scores[classID]
            if confidence > conf: # threshold
                x, y, w, h = output[:4] * np.array([W, H, W, H])
                p0 = int(x - w//2), int(y - h//2)
                p1 = int(x + w//2), int(y + h//2)
                boxes.append([*p0, int(w), int(h)])
                confidences.append(float(confidence))
                classIDs.append(classID)
                # cv.rectangle(img, p0, p1, WHITE, 1)

        indices = cv2.dnn.NMSBoxes(boxes, confidences, conf, conf-0.1)
        if len(indices) > 0:
            for i in indices.flatten():

                (x, y) = (boxes[i][0], boxes[i][1])
                (w, h) = (boxes[i][2], boxes[i][3])

                # Check only for the potted plants:
                if(self.classes[classIDs[i]] in ['potted plant', 'vase', 'pot'] ):
                    print(x,y,w,h,self.classes[classIDs[i]], confidences[i]) # Pixel coordinates of what object (+ confidence)
                    self.plant_pos_pub_x.publish((2*x+w)//2)

                    # Save the image if its a good one!!
        
                    if(time.time() - self.t_pic > 7): # > 5 seconds
                        rospy.logwarn("preSAVE IMAGE1")
                        cv2.imwrite(f'/home/asc03/asclinic-system/catkin_ws/src/asclinic_pkg/src/nodes/yolo/output{self.count}.jpg', img) # Save the output image
                        self.count = self.count + 1
                        rospy.logwarn("postSAVE IMAGE2")
                        self.t_pic = time.time()
                    
                    
                
                
                # # Draw the bounding box onto the image:
                # color = [int(c) for c in self.colors[classIDs[i]]]
                # cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
                # text = "{}: {:.4f}".format(self.classes[classIDs[i]], confidences[i])
                # cv2.putText(img, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 1)
            
    

if __name__ == '__main__':

    rospy.init_node('camera_object_detector')

    camera_object_detector = CameraObjectDetector()

    rospy.spin()