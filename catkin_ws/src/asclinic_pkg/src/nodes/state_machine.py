#!/usr/bin/env python
# -*- coding: utf-8 -*-
############################

import rospy
import random
import sys
import math
import heapq
import csv
import numpy as np
from asclinic_pkg.msg import cmdGoal, path, LeftRightFloat32
from geometry_msgs.msg import Pose2D
from std_msgs.msg import Int32
from std_msgs.msg import UInt32

# Constants

half_wheel_base = 109e-3
wheel_base = 0.17
wheel_radius = 0.072

rotation_state_w = 0.15

frame_width = 1920
frame_lower = 0.2
frame_higher = 0.8

plant_dict = {
    'one': (13.5, 9.5),
    'two': (11.0, 0.5),
    'three': (19.5, 1),
    'four': (19.5, 9)
}

slp_dur = rospy.Duration(0.1)

# Variables
timer = 0.0

# Classes
class state_machine:
    def __init__(self) -> None:
        # initialise variables
        self.odom = Pose2D()
        self.traject_x = []
        self.traject_y = []
        self.traject_num = 0
        self.allow = 2e-1
        self.goal = cmdGoal()

        self.aruco_flag = False
        self.exit_flag = False
        self.plant = 0
        self.plant_pos_x = 0
        
        # initialise sub/pub
        # SUBSCRIBERS
        # odom
        rospy.Subscriber("odometry_pose", Pose2D, self.odomCB)
        # path
        rospy.Subscriber("path", path, self.pathCB)
        # aruco flag
        rospy.Subscriber("aruco_spotted", UInt32, self.arucoCB)
        # Plant in frame pos
        rospy.Subscriber("plant_pos_x", UInt32, self.plantPosCB)
        # scan
        # rospy.Subscriber("topic", type, callback)
        # PUBLISHERS
        # path goal
        self.goal_pub = rospy.Publisher("input_goal", cmdGoal, queue_size=1)
        # odom goal
        self.odom_goal_pub = rospy.Publisher("goal_point", Pose2D, queue_size=10)
        # state machine switch flag
        self.takeover_pub = rospy.Publisher("takeover_flag", Int32, queue_size=1)
        # LR wheel
        self.vel_pub = rospy.Publisher("control_velocity", LeftRightFloat32, queue_size=1)
        # image save request
        self.take_pic_pub = rospy.Publisher("/asc/request_save_image", UInt32, queue_size=1)
        
        # localise
        self.localise()

        pass
    
    # CallBacks
    def odomCB(self, odom):
        self.odom.x = odom.x
        self.odom.y = odom.y
        self.odom.theta = odom.theta
        # print(odom)

    def pathCB(self, path):
        self.traject_num = path.num
        self.traject_x = path.X
        self.traject_y = path.Y
        print(path)

    def arucoCB(self, data):
        self.aruco_flag = True

    def plantPosCB(self, data):
        self.plant_pos_x = data.data

    # States
    def localise(self):
        self.AssumingDirectControl()
        rospy.logwarn("Localising")
        # set velocity to 0 and wait 1 second for odometry to fix itself
        theta_dot_pub = self.vw_to_lr(0, 0)
        self.vel_pub.publish(theta_dot_pub)
        rospy.sleep(slp_dur * 10)
        self.ExitDirectControl()

    def findPath(self, plant):
        rospy.logwarn("Pathing")
        self.goal.x = plant_dict[plant][0]
        self.goal.y = plant_dict[plant][1]
        self.goal_pub.publish(self.goal)
        rospy.sleep(slp_dur)

    def findPlant(self):
        self.AssumingDirectControl()
        rospy.logwarn("Finding Plant")
        while ((self.plant_pos_x < (frame_lower * frame_width)) or (self.plant_pos_x > (frame_higher * frame_width))):
            self.setVel(0,rotation_state_w)
            rospy.sleep(slp_dur)
        self.setVel(0,0)
        rospy.sleep(slp_dur)
        # rotate til plant bounding box is centered, then take picture
        self.takePic()
        rospy.sleep(slp_dur)
        self.findAruco()
        self.localise()
        self.ExitDirectControl()

    def takePic(self):
        self.take_pic_pub.publish(1)
        pass

    def recovery(self):
        self.AssumingDirectControl()
        rospy.sleep(slp_dur)
        self.findAruco()
        self.setVel(0,0)
        rospy.sleep(slp_dur * 3)
        self.localise()

    def driving(self):
        for i in range(1,self.traject_num):
            rospy.logwarn("Driving")
            goal = Pose2D()
            goal.x = self.traject_x[i]
            goal.y = self.traject_y[i]
            ang_diff = self.odom.theta - np.arctan2((goal.y-self.odom.y), (goal.x-self.odom.x))
            
            rospy.logwarn(np.degrees(ang_diff))
            rospy.logerr(np.degrees(self.odom.theta))
            if abs(ang_diff) > np.pi/6:
                self.rotate(np.arctan2((goal.y-self.odom.y), (goal.x-self.odom.x)))
                
            self.odom_goal_pub.publish(goal)
            while(1):
                rospy.sleep(slp_dur)
                dx = self.odom.x - self.traject_x[i]
                dy = self.odom.y - self.traject_y[i]
                if (abs(dx)<self.allow) and (abs(dy)<self.allow):
                    self.localise()
                    break
        rospy.sleep(slp_dur)
        self.traject_num = 0
        self.traject_x = []
        self.traject_y = []

    def findAruco(self):
        rospy.logwarn("Finding Aruco")
        while self.aruco_flag is not True:
            self.setVel(0,rotation_state_w)
            rospy.sleep(slp_dur)
        self.aruco_flag = False

    def rotate(self, t_theta):
        self.AssumingDirectControl()
        rospy.logwarn("Rotating")
        ang_diff = self.ang_diff(self.odom.theta,t_theta)

        

        while (abs(ang_diff)>np.pi/6):

            rospy.logwarn(np.degrees(t_theta))
            rospy.logerr(np.degrees(self.odom.theta))

            if ang_diff > 0:
                self.setVel(0,-rotation_state_w)
            else: self.setVel(0,rotation_state_w)
            ang_diff = self.ang_diff(self.odom.theta,t_theta)
            rospy.sleep(slp_dur)
        self.ExitDirectControl()

    # functions
    def AssumingDirectControl(self):
        self.takeover_pub.publish(1)

    def ExitDirectControl(self):
        self.takeover_pub.publish(0)

    def vw_to_lr(self, v, w):
        theta_dot_pub = LeftRightFloat32()
        theta_dot_pub.left = v/wheel_radius - half_wheel_base*w/wheel_radius
        theta_dot_pub.right = v/wheel_radius + half_wheel_base*w/wheel_radius
        return theta_dot_pub
    
    def setVel(self, v, w):
        theta_dot_pub = self.vw_to_lr(v, w)
        self.vel_pub.publish(theta_dot_pub)

    def ang_diff(self, odom_ang, target_ang):
        return (odom_ang - target_ang)

    # State Machine Loop
    def state_update(self):
        print(plant_dict.keys())
        for plant in plant_dict.keys():
            rospy.sleep(slp_dur)
            self.localise()
            rospy.sleep(slp_dur)
            self.findPath(plant)
            rospy.sleep(slp_dur)
            self.driving()
            rospy.sleep(slp_dur)
            self.findPlant()
            rospy.sleep(slp_dur)
    
# Functions 
# def timerCB(none):
#     FSM.state_update()
#     pass

if __name__ == '__main__':

    node_name = 'state_machine'
    rospy.init_node(node_name)

    # initialise a timer
    # rospy.Timer(rospy.Duration(0.1), timerCB)
    
    # initialise state machine
    FSM = state_machine()
    FSM.state_update()

    rospy.spin()
    


