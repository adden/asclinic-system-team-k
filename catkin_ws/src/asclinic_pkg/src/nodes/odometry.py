#!/usr/bin/env python
"""
Subscribe to /encoder_counts and works out the odometry localisation from that
"""

import rospy
from asclinic_pkg.msg import LeftRightInt32, LeftRightFloat32
import numpy as np
from geometry_msgs.msg import Pose2D
from std_msgs.msg import Float64MultiArray

WHEEL_RADIUS = 72e-3 # r
COUNTS_PER_REV = 2240
WHEEL_BASE = 218e-3 #2b

class OdometryLocalisation:
    def __init__(self):

        # Initialise location at origin for now
        self.x = 14.5 # x position
        self.y = 6.5 # y position
        self.phi = np.pi/2 # heading angle


        self.time = rospy.get_time() # Time


        self.leftForward = 1 # 1 for going forward, -1 for going backwards
        self.rightForward = 1 


        # Initialise subscriber to the encoder counts:
        self.encoder_subscriber = rospy.Subscriber("/encoder_counts", LeftRightInt32, self.encoder_count_callback)

        # Subscriber for wheel direction
        self.wheel_dir_sub = rospy.Subscriber("/control_velocity", LeftRightFloat32, self.dir_update_cb)
        
        # Initialise publisher for the odometry localisation:
        self.odom_publisher = rospy.Publisher('/odometry_pose', Pose2D, queue_size=5)


        # Publisher for the angular velocities, theta_dot, rads/second
        self.theta_dot_pub = rospy.Publisher('theta_dot', LeftRightFloat32, queue_size=5)

        # AruCo for localisation fusion:
        self.aruco_pose_sub = rospy.Subscriber('/aruco_pose', Pose2D, self.aruco_fusion_callback)

        rospy.sleep(rospy.Duration(1))
        


    def encoder_count_callback(self, msg):
        # Encoder tick counts since the previous time step
        left_count = msg.left
        right_count = msg.right
        

        # Change in the motor angle in radians:
        delta_thetaL = left_count * (2*np.pi) / COUNTS_PER_REV
        delta_thetaR = right_count * (2*np.pi) / COUNTS_PER_REV
        # print('el er',left_count,right_count)
        # print('dtl dtr',delta_thetaL,delta_thetaR)
        delta_s = (WHEEL_RADIUS/2) * (delta_thetaL + delta_thetaR)
        delta_phi = (WHEEL_RADIUS/WHEEL_BASE) * (delta_thetaR - delta_thetaL)
        # print('ds dphi',np.degrees(delta_phi))
        

        # Odometry pose update: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.x = self.x + delta_s*np.cos(self.phi + 0.5*delta_phi)
        self.y = self.y + delta_s*np.sin(self.phi + 0.5*delta_phi)
        self.phi = self.phi + delta_phi

        if(self.phi >= np.pi):
            self.phi = self.phi - 2*np.pi
        elif(self.phi <= -np.pi):
            self.phi = self.phi + 2*np.pi

        # Publish the pose:
        self.odom_publisher.publish(Pose2D(self.x, self.y, self.phi))
        




    def dir_update_cb(self, msg):
        # Update whether the left/right wheel is going forward
        self.leftForward = np.sign(msg.left) # 1 for going forward, -1 for going backwards
        self.rightForward = np.sign(msg.right)
        # print(self.leftForward,self.rightForward)



    def aruco_fusion_callback(self, msg: Pose2D):
        """ Whenever the aruco_capture node sends in a pose estimation, we do some sensor fusion magic to update the current pose """
        aruco_x = msg.x
        aruco_y = msg.y
        aruco_phi = msg.theta

        # TODO: Update when stationary in the state machine

        # TODO: THIS GETS MESSY WITH KALMAN FILTER, PROBABLY JUST HARD CODE THE WEIGHTS
        Kt_dash = 0
        Kt = 1 - Kt_dash
        
        self.x = Kt_dash * self.x + Kt * aruco_x
        self.y = Kt_dash * self.y + Kt *aruco_y
        self.phi = Kt_dash * self.phi + Kt *aruco_phi
        # rospy.logwarn("ArUCO UPDATE POSE")


if __name__ == '__main__':
    rospy.init_node('odometry_localisation')

    odometry_localisation = OdometryLocalisation()

    rospy.spin()