#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import rospy
from asclinic_pkg.msg import LeftRightFloat32
# from util import *


node_name_space = None
# PID parameters
kp = 11.5
kd = 0.5
ki = 0.5

time_lim = 20
class Onboard_controller:

    def __init__(self):
        # Initialise a publisher
        self.motor_pwm_publisher = rospy.Publisher("set_motor_duty_cycle", LeftRightFloat32, queue_size=10)
        self.velocity_subscriber = rospy.Subscriber('control_velocity', LeftRightFloat32, self.control_vel_callback, queue_size=5)
        self.odom_vel_subscriber = rospy.Subscriber('theta_dot', LeftRightFloat32, self.odom_pos_callback, queue_size=5) # thetaL_dot and thetaR_dot

        self.control_vel = None
        self.odom_pos = None
        self.unlock_1 = False
        self.unlock_2 = False
        # self.lock = Lock()

        # control variables
        self.pos_target_left = 0
        self.pos_target_right = 0
        self.error_left_dedt = 0
        self.error_right_dedt = 0
        self.error_left_integral = 0
        self.error_right_integral = 0
        self.error_left_prev = 0
        self.error_right_prev = 0

        # timer variables
        self.initial_time = rospy.get_time()
        self.past_time = 0
        self.current_time = 0

    def control_vel_callback(self, msg):
        self.control_vel = msg
        self.unlock_1 = True
        # self.lock.acquire()
        self.publish_pwm()
        # self.lock.release()

    def odom_pos_callback(self, msg):
        self.odom_pos = msg
        self.unlock_2 = True
        # self.lock.acquire()
        self.publish_pwm()
        # self.lock.release()

    # Respond to velocity update request and send corresponding pwm signal to motor
    def publish_pwm(self):
        rospy.loginfo(str(self.unlock_1))
        # rospy.loginfo(str(self.unlock_2))
        if self.unlock_1 and self.unlock_2:
        # if self.unlock_1:
            # time calculation
            self.past_time = self.current_time
            self.current_time = rospy.get_time() - self.initial_time

            # incremental time between encoder readings
            delta_t  = self.current_time - self.past_time
            self.pos_target_left += self.control_vel.left*delta_t
            self.pos_target_right += self.control_vel.right*delta_t

            # error calculation
            error_left = self.pos_target_left - self.odom_pos.left
            error_right = self.pos_target_right - self.odom_pos.right

            # error derivative 
            self.error_left_dedt = (error_left - self.error_left_prev)/delta_t
            self.error_right_dedt = (error_right - self.error_right_prev)/delta_t

            # error integral
            self.error_left_integral = self.error_left_integral + error_left*delta_t
            self.error_right_integral = self.error_right_integral + error_right*delta_t

            # storing past errors
            self.error_left_prev = error_left
            self.error_right_prev = error_right

            # control input
            pwm_duty_cycle = LeftRightFloat32()
            if self.current_time > time_lim:
                pwm_duty_cycle.left = 0
                pwm_duty_cycle.right = 0
            else:
                pwm_duty_cycle.left = np.max([kp*error_left + kd*self.error_left_dedt + ki*self.error_left_integral, 15])
                pwm_duty_cycle.right = np.max([kp*error_right+ kd*self.error_right_dedt + ki*self.error_right_integral, 15])
            # publish pwm signal to motor
            # rospy.loginfo('pwm_l = ' + str(pwm_duty_cycle.left) + ' pwm_r = ' + str(pwm_duty_cycle.right))
            self.motor_pwm_publisher.publish(pwm_duty_cycle)
        self.unlock_1 = False
        self.unlock_2 = False

def shutdown():
    zero_pwm = LeftRightFloat32()
    zero_pwm.Left = 0.0
    zero_pwm.right = 0.0
    Onboard_controller.motor_pwm_publisher.publish(zero_pwm)

if __name__ == '__main__':
    # Initialise the node
    rospy.init_node("onboard_control")
    node_name_space = rospy.get_namespace()
    # Display the namespace of the node handle
    rospy.loginfo("[Onboard control Node] namespace of node = " + node_name_space)
    # Start an instance of the class
    controller = Onboard_controller()
    # Spin as a single-threaded node
    rospy.on_shutdown(shutdown)
    rospy.spin()