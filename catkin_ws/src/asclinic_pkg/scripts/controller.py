#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import numpy as np
from asclinic_pkg.msg import RobotPose, LeftRightFloat32
from util import *


node_name_space = None

# control parameters
k1 = 0.15
k2 = 0.15
l1 = 0.2
l2 = 0.2
a = 0.38


class Offboard_controller:

    def __init__(self, initial_pose):
        # Initialise a publisher
        self.vel_publisher = rospy.Publisher('control_velocity', LeftRightFloat32, queue_size=10)
        self.odom_subscriber = rospy.Subscriber('odom_pose', RobotPose, self.subscriberCallback, queue_size=10)

        # Initialise a time counter
        # Initial time stamp when the node starts
        self.initial_time = rospy.get_time()
        # The past time stamp
        self.past_time = 0
        # Current time the robot has move along the path
        self.t = 0
        # Robot poses
        self.past_pose = initial_pose
        self.current_pose = initial_pose


  # Respond to timer callback
    def subscriberCallback(self, odom_pose):
        # storing time and past poses
        self.past_time = self.t
        self.t = rospy.get_time() - self.initial_time
        self.past_pose = self.current_pose

        # define robot movement
        self.current_pose.move_y(standard_movement_vel*(self.t-self.past_time))
        
        # error difference between reference pose and odom pose
        path_diff = self.current_pose - pose(odom_pose.x, odom_pose.y, odom_pose.phi)
        x_dot_ref = 0
        y_dot_ref = standard_movement_vel
        v_pub, omega_pub = self.ang_ref_control(path_diff, x_dot_ref, y_dot_ref, odom_pose)
        rospy.loginfo('x = ' + str(path_diff.x) + ' y =' + str(path_diff.y) + ' phi =' +str(path_diff.phi))
        # rospy.loginfo('v = ' + str(v_pub) + ' w =' + str(omega_pub))
        # velocity messages for publishing to motor
        theta_dot_pub = LeftRightFloat32()
        theta_dot_pub.left = v_pub/wheel_radius - half_wheel_base*omega_pub/wheel_radius
        theta_dot_pub.right = v_pub/wheel_radius + half_wheel_base*omega_pub/wheel_radius

        # publish velocity to motor
        self.vel_publisher.publish(theta_dot_pub)

  
    # implement non-linear controller for calculating reference angular velocity
    def ang_ref_control(self, path_diff, x_dot_ref, y_dot_ref, odom_pose):
        
        q = x_dot_ref + l1*np.tanh((k1/l1)*path_diff.x)
        p = y_dot_ref + l2*np.tanh((k2/l2)*path_diff.y)

        v = np.cos(odom_pose.phi) * q + np.sin(odom_pose.phi) * p
        w = -(1/a)*np.sin(odom_pose.phi) * q + (1/a)*np.cos(odom_pose.phi) * p

        return v, w




if __name__ == '__main__':
    # Initialise the node
    rospy.init_node("controller")
    node_name_space = rospy.get_namespace()
    # Display the namespace of the node handle
    # rospy.loginfo("[Control Node] namespace of node = " + node_name_space)
    # Start an instance of the class
    controller = Offboard_controller(pose(x_starting_position, y_starting_position, phi_starting_position))
    # Spin as a single-threaded node
    rospy.spin()