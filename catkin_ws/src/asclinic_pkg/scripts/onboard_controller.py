#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import rospy
from asclinic_pkg.msg import LeftRightFloat32
# from util import *


node_name_space = None
# PID parameters
kp = 11.5
kd = 0.5
ki = 0.5

time_lim = 60

r = 0.072 #radius of wheel


class Onboard_controller:

    def __init__(self):
        # Initialise a publisher
        self.motor_pwm_publisher = rospy.Publisher("set_motor_duty_cycle", LeftRightFloat32, queue_size=10)
        self.velocity_subscriber = rospy.Subscriber('control_velocity', LeftRightFloat32, self.control_vel_callback, queue_size=5)
        # self.odom_vel_subscriber = rospy.Subscriber('theta_dot', LeftRightFloat32, self.odom_pos_callback, queue_size=5) # thetaL_dot and thetaR_dot

        self.control_vel = None
        self.odom_pos = None
        self.unlock_1 = False
        self.unlock_2 = False
        # self.lock = Lock()

        # control variables
        self.pos_target_left = 0
        self.pos_target_right = 0
        self.error_left_dedt = 0
        self.error_right_dedt = 0
        self.error_left_integral = 0
        self.error_right_integral = 0
        self.error_left_prev = 0
        self.error_right_prev = 0

        # timer variables
        self.initial_time = rospy.get_time()
        self.past_time = 0
        self.current_time = 0

    def control_vel_callback(self, msg):
        self.control_vel = msg
        self.publish_pwm()
        # self.lock.release()

    # Respond to velocity update request and send corresponding pwm signal to motor
    def publish_pwm(self):

            self.past_time = self.current_time
            self.current_time = rospy.get_time() - self.initial_time

            # incremental time between encoder readings
            # delta_t  = self.current_time - self.past_time
            # self.pos_target_left += self.control_vel.left*delta_t
            # self.pos_target_right += self.control_vel.right*delta_t

            # control input
            pwm_duty_cycle = LeftRightFloat32()
            vl = abs(self.control_vel.left) * r
            vr = abs(self.control_vel.right) * r
            pwm_duty_cycle.left = 1005 * (vl**2) + 246.9 * vl + 7.933
            pwm_duty_cycle.right = 991.2 * (vr**2) + 231.3 * vr + 8.018

            if self.control_vel.left < 0: pwm_duty_cycle.left = -pwm_duty_cycle.left
            if self.control_vel.right < 0: pwm_duty_cycle.right = -pwm_duty_cycle.right

            if abs(pwm_duty_cycle.left) < 5:
                pwm_duty_cycle.left = 0 
            elif pwm_duty_cycle.left > 80:
                pwm_duty_cycle.left = 80
            elif pwm_duty_cycle.left < -80:
                pwm_duty_cycle.left = -80

            if abs(pwm_duty_cycle.right) < 5:
                pwm_duty_cycle.right = 0 
            elif pwm_duty_cycle.right > 80:
                pwm_duty_cycle.right = 80
            elif pwm_duty_cycle.right < -80:
                pwm_duty_cycle.right = -80

            # if self.current_time > time_lim:
            #     pwm_duty_cycle.left = 0
            #     pwm_duty_cycle.right = 0
            # publish pwm signal to motor
            # rospy.loginfo('pwm_l = ' + str(pwm_duty_cycle.left) + ' pwm_r = ' + str(pwm_duty_cycle.right))
            self.motor_pwm_publisher.publish(pwm_duty_cycle)


def shutdown():
    zero_pwm = LeftRightFloat32()
    zero_pwm.Left = 0.0
    zero_pwm.right = 0.0
    Onboard_controller.motor_pwm_publisher.publish(zero_pwm)

if __name__ == '__main__':
    # Initialise the node
    rospy.init_node("onboard_control")
    node_name_space = rospy.get_namespace()
    # Display the namespace of the node handle
    rospy.loginfo("[Onboard control Node] namespace of node = " + node_name_space)
    # Start an instance of the class
    controller = Onboard_controller()
    # Spin as a single-threaded node
    rospy.on_shutdown(shutdown)
    rospy.spin()