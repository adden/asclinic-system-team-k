#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import numpy as np
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Odometry
from asclinic_pkg.msg import LeftRightInt32
from tf.transformations import quaternion_from_euler
import util


class odom_pub_node:

    def __init__(self):
        # objects to store odom
        self.odom_new= Odometry()
        self.odom_new.header.frame_id = "odom"
        self.odom_new.pose.pose.position.x = 0
        self.odom_new.pose.pose.position.y = 0
        self.odom_new.pose.pose.orientation.z = 0
        self.odom_new.twist.twist.linear.x = 0
        self.odom_new.twist.twist.linear.y = 0
        self.odom_new.twist.twist.linear.z = 0
        self.odom_new.twist.twist.angular.x = 0
        self.odom_new.twist.twist.angular.y = 0
        self.odom_new.twist.twist.angular.z = 0

        # initial covariance
        self.current_pose_covariance = np.zeros((3,3))
        self.current_pose_covariance[0][0] = 0.01 ** 2
        self.current_pose_covariance[1][1] = 0.01 ** 2
        self.current_pose_covariance[2][2] = (1.0 * np.pi/1800) ** 2
        self.past_pose_covariance = self.current_pose_covariance
        self.odom_old = Odometry()
        self.odom_old.pose.pose.position.x = 0
        self.odom_old.pose.pose.position.y = 0
        self.odom_old.pose.pose.orientation.z = 0.000001
        self.odom_old.pose.covariance
        # odom for publishing
        self.quatOdom = Odometry()

        # publisher and subscriber
        self.encoder_subscriber = rospy.Subscriber('encoder_counts', LeftRightInt32, self.encoderCallback, queue_size=5)
        self.odom_publilsher = rospy.Publisher('odom', Odometry, queue_size=5)
    
    # function to calculate distance travelled
    def odom_update(self, counts):
        # distance traveled from the wheels
        distance_left = counts.left * util.encoder_ratio
        distance_right = counts.right * util.encoder_ratio
        # distance forward
        delta_s = (distance_left + distance_right) * 0.5
        delta_phi = (distance_right - distance_left) / util.wheel_base
        # average angle traveled during the last cycle
        avg_angle = delta_phi * 0.5 +  self.odom_old.pose.pose.orientation.z

        # make sure angle update stays with in range        
        if avg_angle > np.pi:
            avg_angle -= 2*np.pi
        elif avg_angle < -np.pi:
            avg_angle += 2*np.pi

        # calculate new odom pose
        self.odom_new.pose.pose.position.x = self.odom_old.pose.pose.position.x + np.cos(avg_angle)*delta_s
        self.odom_new.pose.pose.position.y = self.odom_old.pose.pose.position.y + np.sin(avg_angle)*delta_s
        self.odom_new.pose.pose.orientation.z = self.odom_old.pose.pose.orientation.z + np.cos(avg_angle)*delta_s

        # prevent lockup from a single bad cycle
        if np.isnan(self.odom_new.pose.pose.position.x) or np.isnan(self.odom_new.pose.pose.position.y) or \
           np.isnan(self.odom_new.pose.pose.orientation.z):
            self.odom_new.pose.pose.position.x = self.odom_old.pose.pose.position.x
            self.odom_new.pose.pose.position.y = self.odom_old.pose.pose.position.y 
            self.odom_new.pose.pose.orientation.z = self.odom_old.pose.pose.orientation.z
        
        # make sure the angle updates in the correct range
        if self.odom_new.pose.pose.orientation.z > np.pi:
            self.odom_new.pose.pose.orientation.z -= 2*np.pi
        elif self.odom_new.pose.pose.orientation.z < -np.pi:
            self.odom_new.pose.pose.orientation.z += 2*np.pi

        # compute velocity
        self.odom_new.header.stamp = rospy.Time.now()
        self.odom_new.twist.twist.linear.x = delta_s/(self.odom_new.header.stamp.to_sec() - self.odom_old.header.stamp.to_sec())
        self.odom_new.twist.twist.angular.z = delta_phi/(self.odom_new.header.stamp.to_sec() - self.odom_old.header.stamp.to_sec())

        # save the odom as past data
        self.odom_old.pose.pose.position.x = self.odom_new.pose.pose.position.x
        self.odom_old.pose.pose.position.y = self.odom_new.pose.pose.position.y
        self.odom_old.pose.pose.orientation.z  = self.odom_new.pose.pose.orientation.z 
        self.odom_old.header.stamp = self.odom_new.header.stamp

    def construct_covariance(self):
        # initialise covariance
        for i in range(36):
            if i == 14 or i == 21 or i == 28:
                self.quatOdom.pose.covariance[i] = 1e3
            else:
                self.quatOdom.pose.covariance[i] = 0




    def publish_quat(self):
        q = quaternion_from_euler(0, 0 , self.odom_new.pose.pose.orientation.z)
        self.quatOdom.header.stamp = self.odom_new.header.stamp
        self.quatOdom.header.frame_id = "odom"
        self.quatOdom.child_frame_id =  "base_footprint"
        self.quatOdom.pose.pose.position.x = self.odom_new.pose.pose.position.x
        self.quatOdom.pose.pose.position.y = self.odom_new.pose.pose.position.y
        self.quatOdom.pose.pose.position.z = self.odom_new.pose.pose.position.z
        self.quatOdom.pose.pose.orientation.x = q[0]
        self.quatOdom.pose.pose.orientation.y = q[1]
        self.quatOdom.pose.pose.orientation.z = q[2]
        self.quatOdom.pose.pose.orientation.w = q[3]
        self.quatOdom.twist.twist.linear.x = self.odom_new.twist.twist.linear.x
        self.quatOdom.twist.twist.linear.y = self.odom_new.twist.twist.linear.y
        self.quatOdom.twist.twist.linear.z = self.odom_new.twist.twist.linear.z
        self.quatOdom.twist.twist.angular.x = self.odom_new.twist.twist.angular.x
        self.quatOdom.twist.twist.angular.y = self.odom_new.twist.twist.angular.y
        self.quatOdom.twist.twist.angular.z = self.odom_new.twist.twist.angular.z

    # Respond to pose update from the hector slam mapping
    def encoderCallback(self, counts):
        self.odom_update(counts)

if __name__ == '__main__':
    rospy.init_node("odom_pub")
    node_name_space = rospy.get_namespace()
    rospy.loginfo("[odom_pub] namespace of node = " + node_name_space)
    odom_pub_node = odom_pub_node()
    rospy.spin()