import numpy as np
from asclinic_pkg.msg import RobotPose

# starting position of the robot
x_starting_position = 0
y_starting_position = 0
phi_starting_position = np.pi/2

# target velocity (unit m/s)
standard_movement_vel = 0.1

# robot information (unit : m)
half_wheel_base = 0.085
wheel_base = 0.17
wheel_radius = 0.072
encoder_ratio = 0.0002022

# initial covariance for odom update
# for [x, y, phi]^T
odom_pose_cov_initial = np.array([[0.01**2, 0, 0],
                                  [0, 0.01**2, 0],
                                  [0, 0, (np.pi/1800)**2]])

# initial angular velocity variance
odom_theta_l_var = (0.1)**2
odom_theta_r_var = (0.1)**2

# Convert enconder count to velocity
encoder_to_vel_ratio = 0.000101

# pose index position
x_index = 0
y_index = 1
phi_index = 2

# encoder publishing time interval
m_delta_t_for_publishing_counts = 0.1


# forgetting factor for digital filter
alpha = 0.125

# This class stores the pose information [x, y, phi]^T of the robot
class pose:
    
    def __init__(self, x, y, phi):
        self.x = x
        self.y = y
        self.phi = phi

    def __add__(self, other):
        x = self.x + other.x
        y = self.y + other.y
        phi = self.phi + other.phi
        return pose(x, y, phi)

    def __sub__(self, other):
        x = self.x - other.x
        y = self.y - other.y
        phi = self.phi - other.phi
        return pose(x, y, phi)

    def move_x(self, dist):
        self.x += dist

    def move_y(self, dist):
        self.y += dist

    def move_phi(self, angle):
        self.phi += angle

    def convert_pose(self):
        pose_msg = RobotPose()
        pose_msg.x = self.x
        pose_msg.y = self.y
        pose_msg.phi = self.phi
        return pose_msg
