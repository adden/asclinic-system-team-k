#!/usr/bin/env python
# -*- coding: utf-8 -*-

# https://answers.ros.org/question/192682/navigation-cmd_vel-and-twiststamped/
import rospy
from geometry_msgs.msg import Twist, TwistStamped
import time

class cmd_vel_stamped_node:

    def __init__(self):
        # Initialise a publisher
        self.vel_publisher = rospy.Publisher('cmd_vel_stamped', TwistStamped, queue_size=10)
        self.vel_subscriber = rospy.Subscriber('cmd_vel', Twist, self.poseCallback, queue_size=10)
        
    # Respond to pose update from the hector slam mapping
    def poseCallback(self, cmd_vel):

        # inherit the previous velocity messages
        cmd_vel_stamped = TwistStamped()
        cmd_vel_stamped.twist = cmd_vel

        # add time stamp to the velocity message
        now = rospy.get_rostime()
        cmd_vel_stamped.header.stamp.secs = now.secs
        cmd_vel_stamped.header.stamp.nsecs = now.nsecs
        self.vel_publisher.publish(cmd_vel_stamped)

if __name__ == '__main__':
    rospy.init_node("cmd_vel_stamped")
    node_name_space = rospy.get_namespace()
    rospy.loginfo("[cmd_vel stamped node] namespace of node = " + node_name_space)
    cmd_vel_stamped_node = cmd_vel_stamped_node()
    rospy.spin()