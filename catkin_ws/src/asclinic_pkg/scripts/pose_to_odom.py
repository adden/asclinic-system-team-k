#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from geometry_msgs.msg import PoseWithCovarianceStamped
from nav_msgs.msg import Odometry
import numpy as np


class pose_to_odom_node:

    def __init__(self):
        # Initialise a publisher
        self.odom_publisher = rospy.Publisher('vo', Odometry, queue_size=5)
        self.pose_subscriber = rospy.Subscriber('poseupdate', PoseWithCovarianceStamped, self.poseCallback, queue_size=5)
        
    # Respond to pose update from the hector slam mapping
    def poseCallback(self, poseupdate):
            odom_update = Odometry()
            odom_update.header = poseupdate.header
            odom_update.child_frame_id = "base_footprint"
            odom_update.pose = poseupdate.pose
            covariance = np.zeros(36)
            for i in range(36):
                if i == 0 or i == 7 or i == 14:
                    covariance[i] = 0.001
                elif i == 21 or i == 28 or i == 35:
                    covariance[i] = 1e3
                else: 
                    covariance[i] = 0
            odom_update.pose.covariance = covariance.tolist()
            self.odom_publisher.publish(odom_update)


if __name__ == '__main__':
    rospy.init_node("pose_to_odom")
    node_name_space = rospy.get_namespace()
    rospy.loginfo("[Pose to odom node] namespace of node = " + node_name_space)
    pose_to_odom_node = pose_to_odom_node()
    rospy.spin()