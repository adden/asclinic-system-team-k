#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from asclinic_pkg.msg import LeftRightFloat32
from util import *


node_name_space = None
vel_target = 1.7675
# PID parameters
kp = 11.5
kd = 0.5
ki = 0.5
# test time limit
time_lim = 10
class Motor_controller:

    def __init__(self):
        # Initialise a publisher
        self.motor_pwm_publisher = rospy.Publisher("set_motor_duty_cycle", LeftRightFloat32, queue_size=10)
        self.velocity_subscriber = rospy.Subscriber('ang_position', LeftRightFloat32, self.subscriberCallback, queue_size=10)

        self.pos_target = 0

        # error variables
        self.error_left_dedt = 0
        self.error_right_dedt = 0
        self.error_left_integral = 0
        self.error_right_integral = 0
        self.error_left_prev = 0
        self.error_right_prev = 0

        # timer variables
        self.initial_time = rospy.get_time()
        self.past_time = 0
        self.current_time = 0

    # Respond to velocity update request and send corresponding pwm signal to motor
    def subscriberCallback(self, velocities):

        # time calculation
        self.past_time = self.current_time
        self.current_time = rospy.get_time() - self.initial_time
    
        # incremental time between encoder readings
        delta_t  = self.current_time - self.past_time
        self.pos_target += vel_target*delta_t

        # error values between target angular velocities and motor encoder velocities
        error_left = self.pos_target - velocities.left
        error_right = self.pos_target - velocities.right

        # error derivative 
        self.error_left_dedt = (error_left - self.error_left_prev)/delta_t
        self.error_right_dedt = (error_right - self.error_right_prev)/delta_t

        # error integral
        self.error_left_integral = self.error_left_integral + error_left*delta_t
        self.error_right_integral = self.error_right_integral + error_right*delta_t

        # storing past errors
        self.error_left_prev = error_left
        self.error_right_prev = error_right

        # control input
        pwm_duty_cycle = LeftRightFloat32()
        if self.current_time > time_lim:
            pwm_duty_cycle.left = 0
            pwm_duty_cycle.right = 0
        else:
            pwm_duty_cycle.left = kp*error_left + kd*self.error_left_dedt + ki*self.error_left_integral
            pwm_duty_cycle.right = kp*error_right+ kd*self.error_right_dedt + ki*self.error_right_integral
        # publish pwm signal to motor
        self.motor_pwm_publisher.publish(pwm_duty_cycle)


if __name__ == '__main__':
    # Initialise the node
    rospy.init_node("Motor_control")
    node_name_space = rospy.get_namespace()
    # Display the namespace of the node handle
    rospy.loginfo("[Motor control Node] namespace of node = " + node_name_space)
    # Start an instance of the class
    controller = Motor_controller()
    # Spin as a single-threaded node
    rospy.spin()