#!/usr/bin/env python
# -*- coding: utf-8 -*-

from geometry_msgs.msg import Pose2D
import rospy
import numpy as np
from asclinic_pkg.msg import RobotPose, LeftRightFloat32
from std_msgs.msg import Int32
# from util import pose

# rostopic pub /goal_point geometry_msgs/Pose2D "{x: 1.0, y:0.0}" 


# starting position of the robot
x_starting_position = 14.5
y_starting_position = 6.5
theta_starting_position = np.pi/2

# target velocity (unit m/s)
standard_movement_vel = 0.1

# robot information (unit : m)
half_wheel_base = 109e-3
wheel_base = 0.17
wheel_radius = 0.072
# encoder_ratio = 0.0002022

# initial covariance for odom update
# for [x, y, theta]^T
odom_pose_cov_initial = np.array([[0.01**2, 0, 0],
                                  [0, 0.01**2, 0],
                                  [0, 0, (np.pi/1800)**2]])

# initial angular velocity variance
odom_theta_l_var = (0.1)**2
odom_theta_r_var = (0.1)**2

# Convert enconder count to velocity
encoder_to_vel_ratio = 0.000101

# pose index position
x_index = 0
y_index = 1
theta_index = 2

# encoder publishing time interval
m_delta_t_for_publishing_counts = 0.1


# forgetting factor for digital filter
alpha = 0.125

# control parameters
k1 = 0.1
k2 = 0.1
l1 = 0.06
l2 = 0.06
a = 0.38

class pose:
    
    def __init__(self, x, y, theta):
        self.x = x
        self.y = y
        self.theta = theta

    def __add__(self, other):
        x = self.x + other.x
        y = self.y + other.y
        theta = self.theta + other.theta
        return pose(x, y, theta)

    def __sub__(self, other):
        x = self.x - other.x
        y = self.y - other.y
        theta = self.theta - other.theta
        return pose(x, y, theta)

    def move_x(self, dist):
        self.x += dist

    def move_y(self, dist):
        self.y += dist

    def move_theta(self, angle):
        self.theta += angle

    def convert_pose(self):
        pose_msg = RobotPose()
        pose_msg.x = self.x
        pose_msg.y = self.y
        pose_msg.theta = self.theta
        return pose_msg

class Offboard_controller:

    def __init__(self, initial_pose):
        # Initialise a publisher
        self.vel_publisher = rospy.Publisher('control_velocity', LeftRightFloat32, queue_size=10)
        self.odom_subscriber = rospy.Subscriber('odometry_pose', Pose2D, self.subscriberCallback, queue_size=10)
        self.goalpoint_subscriber = rospy.Subscriber('goal_point', Pose2D, self.pointupdate, queue_size=10)
        self.flag_subscriber = rospy.Subscriber('takeover_flag', Int32, self.FLagForRotation, queue_size=10)

        # Initialise a time counter
        # Initial time stamp when the node starts
        self.initial_time = rospy.get_time()
        # The past time stamp
        self.past_time = 0
        # Current time the robot has move along the path
        self.t = 0
        # Robot poses
        self.past_pose = initial_pose
        self.current_goal = initial_pose
        self.flag = 0

    def pointupdate(self, goal_pose):
        self.current_goal.x = goal_pose.x
        self.current_goal.y = goal_pose.y

    def FLagForRotation(self, flagval):
        self.flag = flagval.data
                
  # Respond to timer callback
    def subscriberCallback(self, odom_pose):
        # storing time and past poses
        self.past_time = self.t
        self.t = rospy.get_time() - self.initial_time
        # self.past_pose = self.current_pose

        # define robot movement
        # self.current_pose.x = self.current_pose.x + (standard_movement_vel*(self.t-self.past_time))
        # self.current_goal.x = 0.5
        # self.current_goal.y = 0.5
        # error difference between reference pose and odom pose
        # path_diff = self.current_pose - Pose2D(odom_pose.x, odom_pose.y, odom_pose.theta)
        path_diff = Pose2D()
        path_diff.x = self.current_goal.x - odom_pose.x
        path_diff.y = self.current_goal.y - odom_pose.y
        path_diff.theta = self.current_goal.theta - odom_pose.theta

        x_dot_ref = 0
        y_dot_ref = 0

        v_pub, omega_pub = self.ang_ref_control(path_diff, x_dot_ref, y_dot_ref, odom_pose)
        # print(v_pub)
        # print(omega_pub)
        # rospy.loginfo('x = ' + str(path_diff.x) + ' y =' + str(path_diff.y) + ' theta =' +str(path_diff.theta))
        # rospy.loginfo('v = ' + str(v_pub) + ' w =' + str(omega_pub))
        # velocity messages for publishing to motor
        theta_dot_pub = LeftRightFloat32()
        theta_dot_pub.left = v_pub/wheel_radius - half_wheel_base*omega_pub/wheel_radius
        theta_dot_pub.right = v_pub/wheel_radius + half_wheel_base*omega_pub/wheel_radius

        # publish velocity to motor
        if self.flag == 0:
            self.vel_publisher.publish(theta_dot_pub)

    # # implement non-linear controller for calculating reference angular velocity
    # def ang_ref_control(self, path_diff, v_ref, omega_ref):
    #     # projection of error coordinates
    #     error_cord = np.dot(np.array([[np.cos(self.current_pose.theta),   np.sin(self.current_pose.theta),  0], \
    #                                   [-np.sin(self.current_pose.theta),  np.cos(self.current_pose.theta),  0], \
    #                                   [0,                               0,                              1]]), \
    #                                   np.array([path_diff.x, path_diff.y, path_diff.theta]).T)
    #     # control inputs
    #     z_1 = error_cord[y_index]
    #     z_2 = np.sin(error_cord[theta_index])
    #     # rospy.loginfo('error = ' + str(error_cord[theta_index]))
    #     # control design params
    #     # t_s - settling_time
    #     t_s = 2
    #     k_1 = 5/(v_ref*t_s) + 0.5
    #     k_2 = 5/(t_s) + 0.5*v_ref

    #     # control input
    #     u = -v_ref*z_1 - v_ref*k_1*z_2 - k_2*(z_2+k_1*z_1)
    #     # if error_cord[theta_index] < 0 and error_cord[theta_index] > -np.pi/2:
    #     #     omega_dot = -np.abs(u/np.cos(error_cord[theta_index]) + omega_ref)
    #     # else:
    #     omega_dot = u/np.cos(error_cord[theta_index]) + omega_ref
    #     return v_ref, omega_dot


    # # implement non-linear controller for calculating reference angular velocity
    # def ang_ref_control(self, odom_pose):
    #     goal_heading = np.arctan2(self.current_pose.y - odom_pose.y, self.current_pose.x - odom_pose.x)
    #     a = goal_heading - odom_pose.theta

    #     theta = self.normalize_pi(odom_pose.theta - self.current_pose.theta)
    #     b = -theta - a
    #     d = np.sqrt((self.current_pose.x - odom_pose.x)**2 + (self.current_pose.y - odom_pose.y)**2)

    #     a = self.normalize_pi(a)
    #     b = self.normalize_pi(b)

    #     v = kp * d 
    #     w = ka*a + kb*b

    #     return v, w

    # implement non-linear controller for calculating reference angular velocity
    def ang_ref_control(self, path_diff, x_dot_ref, y_dot_ref, odom_pose):
        
        q = x_dot_ref + l1*np.tanh((k1/l1)*path_diff.x)
        p = y_dot_ref + l2*np.tanh((k2/l2)*path_diff.y)

        v = np.cos(odom_pose.theta) * q + np.sin(odom_pose.theta) * p
        w = -(1/a)*np.sin(odom_pose.theta) * q + (1/a)*np.cos(odom_pose.theta) * p
        rospy.loginfo('v %f, w %f',v,w)
        return v, w

    # # The following functions are from
    # # https://github.com/merose/diff_drive/blob/master/src/diff_drive/goal_controller.py
    # # credit gives to Mark Rose https://github.com/merose
    # def normalize_half_pi(self, alpha):
    #     alpha = self.normalize_pi(alpha)
    #     if alpha > np.pi/2:
    #         return alpha - np.pi
    #     elif alpha < -np.pi/2:
    #         return alpha + np.pi
    #     else:
    #         return alpha

    # def normalize_pi(self, alpha):
    #     while alpha > np.pi:
    #         alpha -= 2*np.pi
    #     while alpha < -np.pi:
    #         alpha += 2*np.pi
    #     return alpha

    # def sign(self, x):
    #     if x >= 0:
    #         return 1
    #     else:
    #         return -1



if __name__ == '__main__':
    # Initialise the node
    rospy.init_node("offboard_control")
    node_name_space = rospy.get_namespace()
    # Display the namespace of the node handle
    rospy.loginfo("[Control Node] namespace of node = " + node_name_space)
    # Start an instance of the class
    controller = Offboard_controller(Pose2D(x_starting_position, y_starting_position, theta_starting_position))
    # Spin as a single-threaded node
    rospy.spin()