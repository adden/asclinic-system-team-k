#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   pid_velocity - takes messages on wheel_vtarget 
      target velocities for the wheels and monitors wheel for feedback
      
    Copyright (C) 2012 Jon Stephan. 
     
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import rospy
from asclinic_pkg.msg import LeftRightInt32, LeftRightFloat32
from numpy import array
import util

    
######################################################
######################################################
class PidVelocity():
######################################################
######################################################


    #####################################################
    def __init__(self):
    #####################################################
        rospy.init_node("pid_velocity")
        self.nodename = rospy.get_name()
        rospy.loginfo("%s started" % self.nodename)
        
        ### initialize variables
        self.left_target = 0
        self.right_target = 0
        self.left_motor = 0
        self.right_motor = 0
        self.left_vel = 0
        self.right_vel = 0
        self.left_integral = 0
        self.right_integral = 0
        self.left_error = 0
        self.left_derivative = 0
        self.right_error = 0
        self.right_derivative = 0
        self.left_previous_error = 0
        self.right_previous_error = 0
        self.left_wheel = 0
        self.right_wheel = 0


        self.then = rospy.Time.now()
        self.prev_pid_time = rospy.Time.now()

        # variable to keep count of encoder counts received
        self.ticks_since_target = 0

        ### get parameters #### 
        self.Kp = rospy.get_param('~Kp',10)
        self.Ki = rospy.get_param('~Ki',10)
        self.Kd = rospy.get_param('~Kd',0.001)
        self.out_min = rospy.get_param('~out_min',-50)
        self.out_max = rospy.get_param('~out_max',50)
        self.rate = rospy.get_param('~rate',30)
        self.timeout_ticks = rospy.get_param('~timeout_ticks',5)
        self.ticks_per_meter = rospy.get_param('~ticks_meter', 4945)


        #### subscribers/publishers 
        rospy.Subscriber("encoder_counts", LeftRightInt32, self.wheelCallback) 
        rospy.Subscriber("wheel_vtarget", LeftRightFloat32, self.targetCallback) 
        self.pub_motor = rospy.Publisher('set_motor_duty_cycle',LeftRightFloat32, queue_size=10) 
   
    
            
    #####################################################
    def calcVelocity(self):
    #####################################################
        self.dt_duration = rospy.Time.now() - self.then
        self.dt = self.dt_duration.to_sec()
        self.then = rospy.Time.now()
        #rospy.loginfo("Left caclVelocity dt= %0.5f left_wheel=%0.5f" % (self.dt, self.left_wheel))
        #rospy.loginfo("Right caclVelocity dt=%0.5f right_wheel=%0.5f" % (self.dt, self.right_wheel))
        self.left_vel = self.left_wheel / self.dt
        self.right_vel = self.right_wheel / self.dt
        rospy.loginfo('left: ' + str(self.left_vel) + ' right: ' + str(self.left_vel))
        
        
            
        
    # #####################################################
    # def appendVel(self, left_val, right_val):
    # #####################################################
    #     self.left_prev_vel.append(left_val)
    #     self.left_prev_vel.pop(0)
    #     self.right_prev_vel.append(right_val)
    #     self.right_prev_vel.pop(0)
        
    # #####################################################
    # def calcRollingVel(self):
    # #####################################################
    #     p_left = array(self.left_prev_vel)
    #     p_right = array(self.right_prev_vel)
    #     self.left_vel = p_left.mean()
    #     self.right_vel = p_right.mean()
        
    #####################################################
    def doPid(self):
    #####################################################
        pid_dt_duration = rospy.Time.now() - self.prev_pid_time
        pid_dt = pid_dt_duration.to_sec()
        self.prev_pid_time = rospy.Time.now()
        
        self.left_error = self.left_target - self.left_vel
        self.right_error = self.right_target - self.right_vel

        self.left_integral = self.left_integral + (self.left_error * pid_dt)
        self.right_integral = self.right_integral + (self.right_error * pid_dt)

        self.left_derivative = (self.left_error - self.left_previous_error) / pid_dt
        self.right_derivative = (self.right_error - self.right_previous_error) / pid_dt

        self.left_previous_error = self.left_error
        self.right_previous_error = self.right_error
    
        self.left_motor = (self.Kp * self.left_error) + (self.Ki * self.left_integral) + (self.Kd * self.left_derivative)
        self.right_motor = (self.Kp * self.right_error) + (self.Ki * self.right_integral) + (self.Kd * self.right_derivative)

        # limit left motor
        if self.left_motor > self.out_max:
            self.left_motor = self.out_max
            self.left_integral = self.left_integral - (self.left_error * pid_dt)
        if self.left_motor < self.out_min:
            self.left_motor = self.out_min
            self.left_integral = self.left_integral - (self.left_error * pid_dt)

        if self.right_motor > self.out_max:
            self.right_motor = self.out_max
            self.right_integral = self.right_integral - (self.right_error * pid_dt)
        if self.right_motor < self.out_min:
            self.right_motor = self.out_min
            self.right_integral = self.right_integral - (self.right_error * pid_dt)
      
        if self.left_target == 0:
            self.left_motor = 0
        
        if self.right_target == 0:
            self.right_motor = 0
    
        rospy.loginfo("left_wheel:%0.2f tar:%0.2f err:%0.2f int:%0.2f der:%0.2f ## motor:%d " % 
                      (self.left_vel, self.left_target, self.left_error, self.left_integral, self.left_derivative, self.left_motor))
        rospy.loginfo("right_wheel:%0.2f tar:%0.2f err:%0.2f int:%0.2f der:%0.2f ## motor:%d " % 
                      (self.right_vel, self.right_target, self.right_error, self.right_integral, self.right_derivative, self.right_motor))
    


    #####################################################
    def wheelCallback(self, msg):
    ######################################################
        enc_left = msg.left
        enc_right = msg.right

    
        self.left_wheel = 1.0 * enc_left / (self.ticks_per_meter * util.wheel_radius)
        self.right_wheel = 1.0 * enc_right / (self.ticks_per_meter * util.wheel_radius)
        
        if enc_left > 0 or enc_right > 0:
            rospy.loginfo("enc_left: " + str(enc_left) + " enc_right: " + str(enc_right))

        motor_cmd = LeftRightFloat32()
        if self.ticks_since_target < self.timeout_ticks and self.left_target != 0 and self.right_target != 0:
            self.calcVelocity()
            self.doPid()
            # publish pwm command to the motors
            motor_cmd.left = self.left_motor
            motor_cmd.right = self.right_motor
            self.pub_motor.publish(motor_cmd)
            self.ticks_since_target += 1
        else:
            # reset variables
            self.left_integral = 0
            self.right_integral = 0
            self.left_error = 0
            self.left_derivative = 0
            self.right_error = 0
            self.left_previous_error = 0
            self.right_previous_error = 0
            self.right_derivative = 0

            # reset time
            self.then = rospy.Time.now()
            self.prev_pid_time = rospy.Time.now()
            motor_cmd.left = 0
            motor_cmd.right = 0
            self.pub_motor.publish(motor_cmd)
    
    ######################################################
    def targetCallback(self, msg):
    ######################################################
        self.left_target = msg.left
        self.right_target = msg.right
        self.ticks_since_target = 0
        # rospy.logdebug("-D- %s targetCallback " % (self.nodename))
    
    
if __name__ == '__main__':
    # Initialise the node
    rospy.init_node("pid_velocity")
    node_name_space = rospy.get_namespace()
    # Display the namespace of the node handle
    rospy.loginfo("[PID velocity Node] namespace of node = " + node_name_space)
    # Start an instance of the class
    controller = PidVelocity()
    # Spin as a single-threaded node
    rospy.spin()
