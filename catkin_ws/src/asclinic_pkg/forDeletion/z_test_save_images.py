#!/usr/bin/env python


# Just testing if I can write a node to subscribe to /asc/camera_image


import cv2
import rospy
from sensor_msgs.msg import Image
import numpy as np
from cv_bridge import CvBridge
import os


# Directly call a service from the terminal:
# rosservice call /service_name args

# subscribe to /asc/camera_image
count = 1


# Some notes:
# [1] Launch the node that publishes the image frame:
# roslaunch asclinic_pkg camera_capture.launch

# [2] Save an image from camera_capture.py node
# rostopic pub /asc/request_save_image std_msgs/UInt32 "data: 1"



# Some aruco notes:
# [1] roslaunch asclinic_pkg aruco_capture.launch
# [2] rostopic pub /asc/request_save_image std_msgs/UInt32 "data: 1"



IMAGE_DIR = '/home/asc03/asclinic-system/image_output'


def handle_save_image(shouldcapture: bool):
    try:
        






def image_save_callback(msg: Image):


    # Function that does something when subscriber gets a message
    if count == 1:

        image_frame = CvBridge().imgmsg_to_cv2(msg)

        rospy.loginfo(image_frame.shape)

        image_path = os.path.join(IMAGE_DIR, f"test{count}.jpg")

        cv2.imwrite(image_path, image_frame)
    
        count += 1




if __name__ == '__main__':

    rospy.init_node("save_img") # intialise a node called "save_img"

    sub = rospy.Subscriber("/asc/camera_image", Image, image_save_callback) # Set up the subscriber of the asc/camera_image topic


    rospy.spin()
    
    

