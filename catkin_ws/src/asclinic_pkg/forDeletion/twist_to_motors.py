#!/usr/bin/env python
"""
   twist_to_motors - converts a twist message to motor commands.  Needed for navigation stack
   
   
    Copyright (C) 2012 Jon Stephan. 
     
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import rospy
from asclinic_pkg.msg import LeftRightFloat32
from geometry_msgs.msg import Twist
import util

#############################################################
#############################################################
class TwistToMotors():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        self.w = rospy.get_param("~base_width", 0.17)
        self.left = 0
        self.right = 0
        self.pub_motor = rospy.Publisher('wheel_vtarget', LeftRightFloat32, queue_size=10)
        rospy.Subscriber('cmd_vel', Twist, self.twistCallback)


    #############################################################
    def twistCallback(self,msg):
    #############################################################
        self.dx = msg.linear.x
        self.dr = msg.angular.z
        self.right = (1.0 * self.dx + self.dr * self.w / 2) / util.wheel_radius
        self.left = (1.0 * self.dx - self.dr * self.w / 2) / util.wheel_radius
        # rospy.loginfo("publishing: (%d, %d)", left, right) 
        wheel_vtarget = LeftRightFloat32()
        wheel_vtarget.left = self.left
        wheel_vtarget.right = self.right      
        self.pub_motor.publish(wheel_vtarget)

        
#############################################################
#############################################################
if __name__ == '__main__':
    # Initialise the node
    rospy.init_node("twist_to_motor")
    node_name_space = rospy.get_namespace()
    # Display the namespace of the node handle
    rospy.loginfo("[twist_to_motor Node] namespace of node = " + node_name_space)
    # Start an instance of the class
    controller = TwistToMotors()
    # Spin as a single-threaded node
    rospy.spin()

